#include <iostream>
#define N_Max 10

void SWAPSORT(int mas[N_Max], int n)
{
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (mas[i] > mas[j])
				std::swap(mas[i], mas[j]);

	/*
	int tmp = mas[i];
	mas[i] = mas[j];
	mas[j] = t;
	*/
}

void BUBBLESORT(int mas[N_Max], int n)
{
	for (int i = 1; i < n; i++)
	{
		if (mas[i] >= mas[i - 1])
			continue;
		int j = i - 1;
		while (j >= 0 && mas[j] > mas[j + 1])
		{
			std::swap(mas[j], mas[j + 1]);
			j--;
		}
	}
}

int main()
{
	srand(time(0));
	int mas[N_Max];
	for (int i = 0; i < N_Max; i++)
		mas[i] = rand();

	// SWAP SORT
	BUBBLESORT(mas, N_Max);

	for (int i = 0; i < N_Max; i++)
		std::cout << mas[i] << std::endl;

}